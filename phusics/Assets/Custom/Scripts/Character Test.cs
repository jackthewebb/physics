﻿
using UnityEngine;


public class CharacterTest : MonoBehaviour {
	public Color colorOff = Color.black;
	public Color colorOn = Color.yellow;
	public Material mat;

	void Start() { mat.color = colorOff;}



	private void OnTriggerEnter(Collider other){
		//Destroy(other.gameObject);
		mat.color = colorOn;
	}

	private void OnTriggerExit(){
		mat.color = colorOff;
	}
}
