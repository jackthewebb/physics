﻿using UnityEngine;

public class Player : MonoBehaviour {
	private Vector2 input;
	private Rigidbody rb;
	public float speed = 50;
	void Start(){
		rb = GetComponent<Rigidbody> ();
	
	
	}
	void Update(){ input = new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"))*speed;
	
	}

	void FixedUpdate(){
		rb.velocity = new Vector3 (input.x, 0, input.y);
	}

}
